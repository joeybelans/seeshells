__author__ = '''Dan Kottmann <djkottmann@gmail.com>
                Chris Patten <cpatten@packetresearch.com>'''

from distutils.core import setup

setup(
    name="SeeShells",
    version="0.0.4",
    author='Dan Kottmann',
    author_email='djkottmann@gmail.com',
    packages=['seeshells'],
    scripts=['bin/seeshells'],
    url='https://bitbucket.org/dkottmann/seeshells',
    license='LICENSE.txt',
    description='Monitoring Metasploit over RPC for new shells.',
    install_requires=[
        "docopt >= 0.6.1",
        "msgpack-python==0.4.0",
        "msgpack-rpc-python==0.3.3"
    ],
)
